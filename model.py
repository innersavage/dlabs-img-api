from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

Base = declarative_base()


class Thumbnail(Base):
    __tablename__ = 'images'
    id = Column(Integer, primary_key=True)
    path = Column(String)

    def __repr__(self):
        return "id={}, path={}".format(self.id, self.path)
