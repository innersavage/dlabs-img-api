from io import BytesIO
from PIL import Image
import uuid
import os
from model import Thumbnail
from google.cloud import storage
from google.cloud.storage import Blob


class Img:
    def __init__(self, session, total, bucket_name=None):
        self.bucket_name = bucket_name if bucket_name is not None else os.getenv('BUCKET')
        self.session = session
        self.total = total
        if self.bucket_name is not None:
            self.client = storage.Client()
            self.bucket = self.client.get_bucket(self.bucket_name)

    def get(self, image, x, y):
        if self.bucket_name is not None:
            blob = self.bucket.get_blob(image)
            # Watch out here, image variable is overwritten by BytesIO object
            image = BytesIO()
            blob.download_to_file(image)
        im = Image.open(image)
        im_resized = im.resize((x, y))
        img_io = BytesIO()
        im_resized.save(img_io, 'PNG')
        img_io.seek(0)
        return img_io

    def add(self, image):
        filename = 'img/' + str(uuid.uuid1()) + '.png'
        db_session = self.session()
        if self.bucket_name is not None:
            img_io = BytesIO()
            image.save(img_io, 'PNG')
            img_io.seek(0)
            blob = Blob(filename, self.bucket)
            blob.upload_from_file(img_io)
        else:
            image.save(filename, 'PNG')
        im_record = Thumbnail(path=filename)
        db_session.add(im_record)
        db_session.commit()
        self.total.refresh()
        db_session.close()
        return True
