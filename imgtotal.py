import time
import os
from model import Thumbnail
from sqlalchemy import func
import redis


class ImgTotal:
    def __init__(self, session, redis_enabled=None, expire=600):
        self.count = 0
        self.last_check = 0
        self.session = session
        self.expire = expire
        self.redis_enabled = redis_enabled if redis_enabled is not None else os.getenv('REDIS')
        if self.redis_enabled == '1':
            self.redis_host = os.environ.get('REDISHOST', 'localhost')
            self.redis_port = int(os.environ.get('REDISPORT', 6379))
            self.redis_client = redis.Redis(host=self.redis_host, port=self.redis_port)

    def refresh(self):
        db_session = self.session()
        self.count = db_session.query(func.count(Thumbnail.id)).scalar()
        self.last_check = time.time_ns()
        db_session.close()
        if self.redis_enabled == '1':
            self.redis_client.setex('total', self.expire, self.count)
        return True

    def get(self):
        if self.redis_enabled == '1':
            self.count = self.redis_client.get('total')
            if self.count is None:
                self.refresh()
        else:
            if time.time_ns() > self.last_check + self.expire * 1000000000:
                self.refresh()
        return int(self.count)
