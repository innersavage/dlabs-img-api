# Introduction
Example of RESTful API for random thumbnail creation. Ready to be deployed locally (with default SQLite) or
on GCP AppEngine with optional external database support and/or Redis.

# Local deployment
```
pip install -r requirements.txt
mkdir img
CREATE_SCHEMA=1 uvicorn main:app --reload
```

# AppEngine deployment
Edit `app.yaml` with your configuration and run:
```
gcloud app deploy
```

More in [official documentation](https://cloud.google.com/appengine/docs/standard/python3/config/appref)

# Environment variables
## BUCKET
Used to select GCP Storage Bucket. If you don't want to use bucket remember to create `img` directory and
do not set the variable.

Example:  
```
BUCKET="BUCKET_NAME"
```

## DATABASE
Database configuration. More information in 
[SQLAlchemy documentation](https://docs.sqlalchemy.org/en/13/dialects/index.html). For other dialects than sqlite and 
psycopg2 additional packages installation may be required.

Examples:
```
DATABASE="postgresql+psycopg2://postgres:password@127.0.0.1:5432/postgres"
```

```
DATABASE="sqlite:///db.sqlite"
```

## DATABASE_ARGS
Optional database arguments. Omit if not needed.

Example:
```
DATABASE_ARGS='{"check_same_thread": false}'
```

## CREATE_SCHEMA
Need to be set during first run to create database schema.  

Example:
```
CREATE_SCHEMA=1
```

## REDIS, REDISHOST, REDISPORT
Setting `REDIS=1` let the app know it should use Redis. `REDISTHOST` and `REDISPORT` are self-explaining.

Example:
```
REDIS=1
REDISHOST='127.0.0.1'
REDISPORT='6379'
```
  
  