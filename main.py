from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import StreamingResponse
from PIL import Image, UnidentifiedImageError
import os
import random
import json
# ORM
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from model import Thumbnail
from imgtotal import ImgTotal
from img import Img

# Setup
database = os.getenv('DATABASE')
if database is not None:
    connect_args = os.getenv('DATABASE_ARGS')
    if connect_args not in [None, ""]:
        engine = create_engine(database, connect_args=json.loads(connect_args))
    else:
        engine = create_engine(database)
else:
    engine = create_engine('sqlite:///db.sqlite', echo=True, connect_args={'check_same_thread': False})

if os.getenv('CREATE_SCHEMA') == '1':
    Thumbnail.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
db_session = Session()
total = ImgTotal(Session, expire=300)
img = Img(Session, total)
app = FastAPI()


@app.post("/images")
def add_item(file: UploadFile = File(...)):
    try:
        im = Image.open(file.file)
    except UnidentifiedImageError:
        raise HTTPException(status_code=500,
                            detail="Unidentified image format",
                            headers={"Cache-Control": "no-cache"})
    img.add(im)
    return {"status": "Success"}


@app.get("/images/{x}x{y}")
def read_item(x: int, y: int):
    if total.get() == 0:
        raise HTTPException(status_code=404,
                            detail="No images to create thumbnail",
                            headers={"Cache-Control": "no-cache"})
    offset = random.randint(0, total.get() - 1)
    image = db_session.query(Thumbnail.path).limit(1).offset(offset).one().path
    return StreamingResponse(img.get(image, x, y),
                             media_type="image/png",
                             headers={"Cache-Control": "public, max-age=3600"})
