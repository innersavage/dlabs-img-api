from fastapi.testclient import TestClient
import os

os.environ['DATABASE'] = 'sqlite:///tests.sqlite'
os.environ['DATABASE_ARGS'] = '{"check_same_thread": false}'
os.environ['CREATE_SCHEMA'] = '1'

from main import app, Session, total
from model import Thumbnail

client = TestClient(app)


def test_read_root():
    response = client.get('/')
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}


def test_read_images_get():
    response = client.get('/images/')
    assert response.status_code == 405
    assert response.json() == {"detail": "Method Not Allowed"}


def test_add_image():
    with open('tests/rubberduck.jpeg', 'rb') as f:
        image = {"file": f.read()}
    response = client.post('/images', files=image)
    assert response.status_code == 200
    assert response.json() == {"status": "Success"}


def test_read_thumbnail_if_none_in_db():
    # Delete all thumbnail records in database
    db_session = Session()
    db_session.query(Thumbnail).delete()
    db_session.commit()
    db_session.close()
    total.refresh()
    response = client.get('/images/400x400')
    assert response.status_code == 404
    assert response.json() == {"detail": "No images to create thumbnail"}
    assert 'Cache-Control' in response.headers
    assert response.headers['Cache-Control'] == 'no-cache'


def test_add_image_and_read_thumbnail():
    test_add_image()
    response = client.get('/images/200x200')
    assert response.status_code == 200
    assert 'Cache-Control' in response.headers
    assert response.headers['Cache-Control'] == 'public, max-age=3600'
